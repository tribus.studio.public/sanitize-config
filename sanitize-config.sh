#!/usr/bin/env bash
VERSION=0.1.2
#
# This utility script can be used for several things:
#  - sanitizing the output of a config sync folder
#  - identifying core.extensions
#  - listing configurations
#  - providing a scriptable tool for exported config management
#
# Color Codes
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
WHITE='\033[1;37m'
NC='\033[0m' # No Color

IGNORE_YMLS=0
REMOVE_CORE=0
REMOVE_FILES=0
SHOW_MODULES=0
HELP=0
WORKING_DIR="$(pwd)"
TARGET_DIR="."
CONFIG_DIR=""

if [[ "$OSTYPE" == "darwin"* ]]
then
  OS_ECHO="-e"
  OS_EXTRA=''
else
  OS_ECHO="-e"
  OS_EXTRA=null
fi

walk_dir () {
    local TARGET_DIR="$2"
    shopt -u nullglob dotglob
    for yml_file in "$1"/*; do
        if [[ -d "$yml_file" ]]
        then
            echo $OS_ECHO "Processing directory ${GREEN}$yml_file${NC}"
            walk_dir "$yml_file" "$TARGET_DIR"
        else
          if [[ -f "$yml_file" ]]
          then
            echo $OS_ECHO "Processing file ${GREEN}$yml_file${NC}"
            if [[ $TARGET_DIR == '.' ]]
            then
              if [[ "$OSTYPE" == "darwin"* ]]
              then
                sed -i '' '/^uuid: /d' $yml_file
                sed -i '' '/  default_config_hash: /d' $yml_file
                sed -i '' '/_core:/d' $yml_file
              else
                sed -i  '/^uuid: /d' $yml_file
                sed -i  '/  default_config_hash: /d' $yml_file
                sed -i  '/_core:/d' $yml_file
              fi
            else
              sed '/^uuid: /d' $yml_file > $TARGET_DIR/$(basename "$yml_file")
              sed '/  default_config_hash: /d' $yml_file > $TARGET_DIR/$(basename "$yml_file")
              sed '/_core:/d' $yml_file > $TARGET_DIR/$(basename "$yml_file")
            fi
          fi
        fi
    done
}

usage () {
  echo $OS_ECHO "${GREEN}Sanitize YAML Configs${NC} for ${GREEN}Drupal${NC} version ${YELLOW}${VERSION}${NC}

  ${YELLOW}Usage:${NC}

    $(basename "$0") [${WHITE}options${NC}] [${WHITE}arguments${NC}] [--]

  ${YELLOW}Arguments:${NC}
    config_path                   The path to the config sync directory where the yaml files are to be sanitized.

  ${YELLOW}Options:${NC}
  ${GREEN} -h, --help${NC}                     Display this help message.
  ${GREEN} -c, --core${NC}                     Remove the core.extensions.yml file.
  ${GREEN} -d, --working-dir=WORKING_DIR${NC}  Change the working directory.
  ${GREEN} -i, --ignore${NC}                   Ignore the processing of the yaml files.
  ${GREEN} -f, --files${NC}                    Remove the file.setting and update.setting files.
  ${GREEN} -m, --modules${NC}                  Display the core.extensions modules and themes files.
  ${GREEN} -t, --target-dir=TARGET_DIR${NC}    Specify the target location for the files.

  ${GREEN}     --${NC}                         End of all options (used as an interrupt).

  "
  exit 2
}

while [ $# -gt 0 ]
do
    case $1 in
        -i|--ignore)
            IGNORE_YMLS=1
            shift
            ;;

        -m|--modules)
            SHOW_MODULES=1
            shift
            ;;

        -c|--core)
            REMOVE_CORE=1
            shift
            ;;

        -f|--files)
            REMOVE_FILES=1
            shift
            ;;

        -d|--working-dir)
            WORKING_DIR="$2"
            shift 2
            ;;

        -t|--target-dir)
            TARGET_DIR="$2"
            shift 2
            ;;

        -h|--help)
            usage
            ;;

        --) # End of all options
            shift
            break
            ;;
        -*)
            echo "Error: Unknown option: $1" >&2
            ## or call function display_help
            exit 1 
            ;;
        *)  # No more options
            CONFIG_DIR="$1"
            break
            ;;

    esac
done

if [ $HELP -eq 1 ]
then
  echo 
  exit 1
fi

if [ -z $CONFIG_DIR ]
then
  exit 1
fi

if [ "${CONFIG_DIR:0:1}" == "/" ]
then
  CONFIG_PATH=${CONFIG_DIR}
else
  CONFIG_PATH="${WORKING_DIR}/${CONFIG_DIR}"
fi

# Add working dir to target dir path.
if [[ $TARGET_DIR != '.' && "${TARGET_DIR:0:1}" != "/" ]]
then
  TARGET_DIR="${WORKING_DIR}/${TARGET_DIR}"
fi

if [ ! -d $TARGET_DIR ]
then
  echo $OS_ECHO "Creating ${GREEN}${TARGET_DIR}${NC} folder."
  mkdir $TARGET_DIR
fi

# @todo: Determine how to (if at all) manage /install/ vs. /optional/ directories.

# Sanitize all of the configuration yaml files so that they
# do not include UUID or default_config_hash specifications.
if [ $IGNORE_YMLS -eq 0 ]
then
  walk_dir $CONFIG_PATH $TARGET_DIR
fi

if [ $SHOW_MODULES -eq 1 ]
then
  echo " "
  echo $OS_ECHO "Copy this list and paste it into the ${GREEN}profile.info.yml${NC} file."
  echo "--------------"
  cat $CONFIG_PATH/core.extension.yml | sed -n 's/\(^[ ]\{2\}\)\([a-z_^:]*\)\(: [0-9]*\)/  - \2/p'
  echo "--------------"
fi

# Now, we replace the config path with our target path for the final removal.
if [ $TARGET_DIR != '.' ]
then
  CONFIG_PATH=$TARGET_DIR
fi

# Remove the core.extension.yml as it cannot exist in a profile.
if [ $REMOVE_CORE -eq 1 ]
then
  rm -fR "${CONFIG_PATH}/core.extension.yml"
fi

if [ $REMOVE_FILES -eq 1 ]
then
  # file.setting.yml can have an affect as well
  rm -fR "${CONFIG_PATH}/file.settings.yml"

  # update.setting.yml can have an affect as well
  rm -fR "${CONFIG_PATH}/update.settings.yml"
fi

