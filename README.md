# Drupal YAML Configuration Sanitization applet

## Options

```bash
Sanitize YAML Configs for Drupal version 0.1.1

Usage:

  sanitize-config [options] [arguments] [--]

Arguments:
  config_path                   The path to the config sync directory where the yaml files are to be sanitized.

Options:
 -h, --help                     Display this help message.
 -c, --core                     Remove the core.extensions.yml file.
 -d, --working-dir=WORKING_DIR  Change the working directory [default: location of execution].
 -i, --ignore                   Ignore the processing of the yaml files.
 -f, --files                    Remove the file.setting and update.setting files.
 -m, --modules                  Display the core.extensions modules and themes files.
 -t, --target-dir=TARGET_DIR    Specify the target location for the files.

     --                         End of all options (used as an interrupt).
```

## Installation

Get the file:
```bash
wget https://gitlab.com/tribus.studio.public/sanitize-config/-/raw/main/sanitize-config.sh -O /usr/local/bin/sanitize-config
```

Then make it executable
```bash
chmod +x /usr/local/bin/sanitize-config
```

## Examples

From the root of you project, if you wanted to process from configurations for the sync folder you might:

```bash
sanitize-config config/default/sync
```

This would process from your projects root all files in the ```config/default/sync``` folder, modify and overwrite their contents.

If you wanted to process the files but have them output in a different folder you might do:

```bash
sanitize-config -t html/profiles/custom/my_profile/config/install config/default/sync
```

The same thing but with the ```core.extensions.yml``` deleted:

```bash
sanitize-config -t html/profiles/custom/my_profile/config/install -c config/default/sync
```

If you were located in your ```home/user``` folder and wanted to work with drupal located elsewhere on the drive:

```bash
sanitize-config -d /var/www/html/drupal -t html/profiles/custom/my_profile/config/install config/default/sync
```

What if you want to process the ```core.extensions.yml``` file to place the enabled modules in your ```profile.info.yml```, try:

```bash
sanitize-config config/default/sync -m -i
```

The ```-i``` tells the program to ignore the processing of the ymls configuration files.

